﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace Museums
{
    public class MapComponent_MuseumTracker : MapComponent
    {
        public MapComponent_MuseumTracker(Map map) : base(map)
        {
        }

        public MuseumData GetMuseumData(Room room)
        {
            if (!cachedMuseumData.ContainsKey(room))
            {
                if (room.Role != Common.RoomRole_Museum)
                {
                    throw new Exception($"provided room has role {room.Role.defName}, but must be {Common.RoomRole_Museum.defName}!");
                }
                MuseumData museumData = new MuseumData(room);
                cachedMuseumData.Add(room, museumData);
            }
            return cachedMuseumData.TryGetValue(room);
        }
        public IEnumerable<Room> Museums => map.regionGrid.allRooms.Where(r => r.Role == Common.RoomRole_Museum);
        Dictionary<Room, MuseumData> cachedMuseumData = new Dictionary<Room, MuseumData>();
    }

    public class MuseumData
    {
        Room room;
        public MuseumData(Room museumRoom)
        {
            this.room = museumRoom;
        }

        public AcceptanceReport CanBeToured
        {
            get
            {
                if (!WatchableBuildings.Any())
                {
                    return "Museums_InspectString_InvalidTourReason_NoWatchableBuilding".Translate();
                }
                return true;
            }
        }

        public Thing GatheringSpot => room.ContainedAndAdjacentThings.FirstOrDefault(t => t.def == Common.GatheringSpot);

        public IEnumerable<Building> WatchableBuildings
        {
            get
            {
                foreach (Thing thing in room.ContainedAndAdjacentThings)
                {
                    if(!(thing is Building building))
                    {
                        continue;
                    }
                    ThingComp_WatchableMuseumBuilding watchableComp = building.TryGetComp<ThingComp_WatchableMuseumBuilding>();
                    if(watchableComp == null)
                    {
                        continue;
                    }
                    if (!watchableComp.IsWatchable)
                    {
                        continue;
                    }

                    yield return building;
                }
            }
        }

        public IEnumerable<Building> SittableBuildings
        {
            get
            {
                foreach (Thing thing in room.ContainedAndAdjacentThings)
                {
                    if (thing is Building building && building.def.building.isSittable)
                    {
                        yield return building;
                    }
                }
            }
        }
    }
}
