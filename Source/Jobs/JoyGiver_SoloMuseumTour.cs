﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.Noise;

namespace Museums
{
    public class JoyGiver_SoloMuseumTour : JoyGiver
    {
        public override Job TryGiveJob(Pawn pawn)
        {
            MapComponent_MuseumTracker tracker = pawn.Map.GetComponent<MapComponent_MuseumTracker>();
            IEnumerable<Room> museums = tracker.Museums;
            if (museums.EnumerableNullOrEmpty())
            {
                return null;
            }

            Room museumRoom = museums.RandomElementWithFallback();
            if (museumRoom == null)
            {
                return null;
            }

            MuseumData museumData = tracker.GetMuseumData(museumRoom);
            if (museumData == null)
            {
                return null;
            }

            if (!museumData.CanBeToured)
            {
                return null;
            }

            Thing gatheringSpot = museumData.GatheringSpot;
            if (gatheringSpot == null)
            {
                return null;
            }

            // validate that the position of the gathering spot is allowed. That implicitly means the position of the spot can be reached through allowed areas and means the pawn can reach the museum room
            if (gatheringSpot.Position.IsForbidden(pawn))
            {
                return null;
            }

            Job job = JobMaker.MakeJob(Common.SoloMuseumTourJob, gatheringSpot);
            return job;
        }
    }
}
