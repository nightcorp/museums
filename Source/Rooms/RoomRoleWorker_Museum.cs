﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace Museums
{
    public class RoomRoleWorker_Museum : RoomRoleWorker
    {
        public override float GetScore(Room room)
        {
            List<Thing> containedAndAdjacentThings = room.ContainedAndAdjacentThings;

            float score = 0;
            bool isValidMuseum = false;
            foreach (Thing thing in containedAndAdjacentThings)
            {
                // at least one gathering spot must exist in the room
                isValidMuseum |= thing.def == Common.GatheringSpot;
                if (thing.TryGetComp<ThingComp_WatchableMuseumBuilding>() != null)
                {
                    score += 8;
                }
            }
            return isValidMuseum ? score : 0;
        }
    }
}
