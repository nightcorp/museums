﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace Museums
{
    public class LearningGiver_MuseumTouring : LearningGiver
    {
        public override bool CanDo(Pawn pawn)
        {
            if (!base.CanDo(pawn))
            {
                return false;
            }
            MapComponent_MuseumTracker museumTracker = pawn.Map.GetComponent<MapComponent_MuseumTracker>();
            if(museumTracker == null)
            {
                return false;
            }
            return museumTracker.Museums.Any();
        }

        public override Job TryGiveJob(Pawn pawn)
        {
            Map map = pawn.Map;
            MapComponent_MuseumTracker comp = map.GetComponent<MapComponent_MuseumTracker>();
            Room museumRoom = comp.Museums.RandomElementWithFallback();
            if (museumRoom == null)
            {
                return null;
            }
            MuseumData museumData = comp.GetMuseumData(museumRoom);
            if (museumData == null)
            {
                return null;
            }
            Thing gatheringSpot = museumData.GatheringSpot;
            if (gatheringSpot == null)
            {
                return null;
            }
            return JobMaker.MakeJob(Common.SoloMuseumTourJob, gatheringSpot);
        }
    }
}
