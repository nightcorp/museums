﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace Museums
{

    [DefOf]
    public static class Museums_ThoughtDefOf
    {
        static Museums_ThoughtDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(Museums_ThoughtDefOf));
        }
        public static ThoughtDef Museums_AttendedGuidedMuseumTour;
        public static ThoughtDef Museums_OrganizedGuidedMuseumTour;
    }

    [DefOf]
    public static class Museums_TaleDefOf
    {
        static Museums_TaleDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(Museums_TaleDefOf));
        }
        public static TaleDef Museums_AttendedGuidedMuseumTour;
        public static TaleDef Museums_OrganizedGuidedMuseumTour;
    }
}
