﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace Museums.WorkGivers
{
    /// <summary>
    /// Based on <see cref="WorkGiver_EmptyEggBox"/>, but for generic thing containers
    /// </summary>
    /// <typeparam name="T">Applicable subtype of <see cref="CompThingContainer"/></typeparam>
    public abstract class WorkGiver_EmptyThingContainer<T> : WorkGiver_Scanner where T : CompThingContainer
    {
        public override abstract ThingRequest PotentialWorkThingRequest { get; }

        public override bool HasJobOnThing(Pawn pawn, Thing t, bool forced = false)
        {
            if(!(t is ThingWithComps thingWithComps))
            {
                return false;
            }
            if (!t.Spawned || t.IsForbidden(pawn))
            {
                return false;
            }
            if (!pawn.CanReserve(t, 1, -1, null, forced))
            {
                return false;
            }
            T containerComp = thingWithComps.GetComp<T>();
            if (((containerComp != null) ? containerComp.ContainedThing : null) == null)
            {
                return false;
            }
            if (containerComp.Empty && !forced)
            {
                return false;
            }
            if (containerComp.ContainedThing.stackCount < containerComp.Props.minCountToEmpty)
            {
                return false;
            }
            if (!StoreUtility.TryFindBestBetterStorageFor(containerComp.ContainedThing, pawn, pawn.Map, StoragePriority.Unstored, pawn.Faction, out _, out _, false))
            {
                JobFailReason.Is(HaulAIUtility.NoEmptyPlaceLowerTrans);
                return false;
            }
            return true;
        }

        public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false)
        {
            T containerComp = t.TryGetComp<T>();
            if (((containerComp != null) ? containerComp.ContainedThing : null) == null)
            {
                return null;
            }
            if (containerComp.Empty && !forced)
            {
                return null;
            }
            if (containerComp.ContainedThing.stackCount < containerComp.Props.minCountToEmpty)
            {
                return null;
            }
            IntVec3 intVec;
            if (!StoreUtility.TryFindBestBetterStorageFor(containerComp.ContainedThing, pawn, pawn.Map, StoragePriority.Unstored, pawn.Faction, out intVec, out _, true))
            {
                JobFailReason.Is(HaulAIUtility.NoEmptyPlaceLowerTrans, null);
                return null;
            }
            Job job = JobMaker.MakeJob(JobDefOf.EmptyThingContainer, t, containerComp.ContainedThing, intVec);
            job.count = containerComp.ContainedThing.stackCount;
            return job;
        }
    }
}
