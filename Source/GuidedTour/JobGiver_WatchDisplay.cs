﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace Museums
{
    public class JobGiver_WatchDisplay : JobGiver_GotoTravelDestination
    {
        protected override LocalTargetInfo GetDestination(Pawn pawn)
        {
            IntVec3 watchCell = GetWatchCellFor(pawn, out Building building);
            //Log.Message($"Pawn {pawn.LabelShort} has watch cell: {watchCell}");

            if (!watchCell.IsValid)
            {
                // returning invalid cell means the think tree skips this node and falls through to the next one
                return IntVec3.Invalid;
            }
            pawn.mindState.duty.focus = watchCell;
            pawn.mindState.duty.overrideFacing = WatchDisplayUtility.RotationToLookAt(watchCell.ToVector3Shifted(), building.TrueCenter());
            return watchCell;

        }

        private IntVec3 GetWatchCellFor(Pawn pawn, out Building watchBuilding)
        {
            LordJob_Joinable_GuidedMuseumTour tourJob = pawn.GetLord().LordJob as LordJob_Joinable_GuidedMuseumTour;
            watchBuilding = tourJob.TourData.CurrentTourFocusBuilding;
            
            if (pawn == tourJob.Organizer)
            {
                return tourJob.TourData.CurrentGuidePosition;
            }

            return watchBuilding.GetComp<ThingComp_WatchableMuseumBuilding>().AllCurrentlyValidWatchCellsFor(pawn).RandomElementWithFallback(IntVec3.Invalid);
        }
    }
}
