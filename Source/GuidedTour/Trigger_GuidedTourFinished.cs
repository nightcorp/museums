﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace Museums.GuidedTour
{
    public class Trigger_GuidedTourFinished : Trigger
    {
        public override bool ActivateOn(Lord lord, TriggerSignal signal)
        {
            if (!(lord.LordJob is LordJob_Joinable_GuidedMuseumTour job))
            {
                return false;
            }
            return job.TourData.HasTourEnded;
        }
    }
}
