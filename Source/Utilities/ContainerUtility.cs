﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace Museums
{
    /// <summary>
    /// Intended to be patched and extended by uxiliary mod integrations if those mods implement non-CompThingContainer comps (like LWM_DeepStorage)
    /// </summary>
    public class ContainerUtility
    {
        /// <summary>
        /// Mods may add their own implementation of containers, to prevent hard dependencies and lazy loading, this method will be patched by integrations for those mods.
        /// </summary>
        public static bool ContainsItem(Thing thing)
        {
            // no idea if it would be possible/smart for a building to implement multiple ways of holding items, but hey, future proofing.

            if (thing is IThingHolder thingHolder)
            {
                if (thingHolder.GetDirectlyHeldThings().Any())
                {
                    return true;
                }
            }

            // covers Building_Storage
            if(thing is ISlotGroupParent slotGroupParent)
            {
                if (slotGroupParent.GetSlotGroup().HeldThings.Any())
                {
                    return true;
                }
            }

            if(thing is ThingWithComps thingWithComps)
            {
                foreach (ThingComp comp in thingWithComps.AllComps)
                {
                    if (comp is IThingHolder compHolder)
                    {
                        if (compHolder.GetDirectlyHeldThings().Any())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public static bool IsContainer(ThingDef thingDef)
        {
            if (typeof(IThingHolder).IsAssignableFrom(thingDef.thingClass))
            {
                return true;
            }
            // covers Building_Storage
            if (typeof(ISlotGroupParent).IsAssignableFrom(thingDef.thingClass))
            {
                return true;
            }
            if (thingDef.HasAssignableCompFrom(typeof(IThingHolder)))
            {
                return true;
            }
            return false;
        }

        public static IEnumerable<Thing> GetAllContents(Thing thing)
        {
            if (thing is IThingHolder thingHolder)
            {
                foreach (Thing innerThing in thingHolder.GetDirectlyHeldThings())
                {
                    yield return thing;
                }
            }

            // covers Building_Storage
            if (thing is ISlotGroupParent slotGroupParent)
            {
                foreach (Thing innerThing in slotGroupParent.GetSlotGroup().HeldThings)
                {
                    yield return innerThing;
                }
            }

            if(thing is ThingWithComps thingWithComps)
            {
                foreach (ThingComp comp in thingWithComps.AllComps)
                {
                    if (comp is IThingHolder compHolder)
                    {
                        foreach (Thing innerThing in compHolder.GetDirectlyHeldThings())
                        {
                            yield return innerThing;
                        }
                    }
                }
            }
        }
    }
}
