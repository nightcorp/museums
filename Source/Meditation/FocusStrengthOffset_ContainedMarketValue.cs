﻿using HarmonyLib;
using Mono.Unix.Native;
using NightmareCore;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Museums
{
    public class FocusStrengthOffset_ContainedMarketValue : FocusStrengthOffset_Curve
    {
        protected override string ExplanationKey => "Museums_StatExplanation_MeditationOffsetFromMarketValue";

        protected override float SourceValue(Thing parent)
        {
            if(!ContainerUtility.ContainsItem(parent))
            {
                return 0f;
            }
            return ContainerUtility.GetAllContents(parent).Sum(t => t.GetStatValue(StatDefOf.MarketValue));
        }

        public override string InspectStringExtra(Thing parent, Pawn user = null)
        {
            if (!ContainerUtility.ContainsItem(parent))
            {
                return "";
            }
            return GetExplanation(parent);
        }

        public override string GetExplanation(Thing parent)
        {
            float containedMarketValue = 0;
            if (ContainerUtility.ContainsItem(parent))
            {
                containedMarketValue = ContainerUtility.GetAllContents(parent).Sum(t => t.GetStatValue(StatDefOf.MarketValue));
            } 
            NamedArgument marketValueText = containedMarketValue.ToStringByStyle(ToStringStyle.Money).Named("VALUE");
            return $"{ExplanationKey.Translate(marketValueText)}: {GetOffset(parent).ToStringWithSign("0%")}";
        }
    }
}
