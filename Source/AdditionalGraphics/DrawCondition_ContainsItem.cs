﻿using HarmonyLib;
using NightmareCore.AdditionalGraphics;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace Museums
{
    public class DrawCondition_ContainsItem : DrawCondition
    {
        ThingDef thingDef;
        IntRange amountRange = IntRange.zero;

        public override bool ShouldDraw(Thing thing)
        {
            if(!(thing is ThingWithComps thingWithComps))
            {
                return false;
            }
            CompThingContainer containerComp = thingWithComps.GetComp<CompThingContainer>();
            if(containerComp == null)
            {
                return false;
            }
            if(containerComp.ContainedThing == null)
            {
                return false;
            }
            if(containerComp.ContainedThing.def != thingDef)
            {
                return false;
            }
            if(amountRange != IntRange.zero)
            {
                if (containerComp.ContainedThing.stackCount < amountRange.min)
                {
                    return false;
                }
                if (containerComp.ContainedThing.stackCount > amountRange.max)
                {
                    return false;
                }
            }
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (thingDef == null)
            {
                yield return $"Required field \"{nameof(thingDef)}\" is not set";
            }
        }
    }
}
