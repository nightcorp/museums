﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace MuseumsIntegration_Hospitality
{
    public class ThinkNode_Conditional_WouldDonateToMuseum : ThinkNode_Conditional
    {
        protected override bool Satisfied(Pawn pawn)
        {
            if(pawn.GuestStatus != GuestStatus.Guest)
            {
                return false;
            }
            MapComponent_GuestGuidedTourTracker trackerComp = pawn.Map.GetComponent<MapComponent_GuestGuidedTourTracker>();
            if(trackerComp.HasGuestDonated(pawn))
            {
                return false;
            }
            if (pawn.inventory?.innerContainer == null)
            {
                return false;
            }
            bool hasAnySilver = pawn.inventory.innerContainer.Any(t => t.def == ThingDefOf.Silver);
            if (!hasAnySilver)
            {
                return false;
            }
            return true;
        }
    }
}
