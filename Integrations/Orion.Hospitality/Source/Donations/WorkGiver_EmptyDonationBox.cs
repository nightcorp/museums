﻿using HarmonyLib;
using Museums.WorkGivers;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace MuseumsIntegration_Hospitality
{
    public class WorkGiver_EmptyDonationBox : WorkGiver_EmptyThingContainer<CompThingContainer>
    {
        public override ThingRequest PotentialWorkThingRequest => ThingRequest.ForDef(Common.DonationBoxThingDef);
    }
}
