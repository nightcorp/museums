﻿#if !v1_4
using LudeonTK;
#endif
using HarmonyLib;
using Museums;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace MuseumsIntegration_Hospitality
{
    public class JobGiver_DonateToMuseum : ThinkNode_JobGiver
    {
        const float maxSilverToDonate = 100;
        const float ratioOfInventorySilverToDonate = 0.6f;

        protected override Job TryGiveJob(Pawn pawn)
        {
            Thing silverThing = pawn.inventory?.innerContainer?.FirstOrDefault(t => t.def == ThingDefOf.Silver);
            if (silverThing == null)
            {
                return null;
            }
            float silverAmount = silverThing.stackCount * ratioOfInventorySilverToDonate;
            silverAmount = Mathf.Min(silverAmount, maxSilverToDonate);
            int silverAmountInt = Mathf.FloorToInt(silverAmount);
            if (silverAmountInt <= 0)
            {
                return null;
            }

            MapComponent_GuestGuidedTourTracker guestTracker = pawn.Map.GetComponent<MapComponent_GuestGuidedTourTracker>();
            if (guestTracker.HasGuestDonated(pawn))
            {
                return null;
            }

            LordJob_Joinable_GuidedMuseumTour tourJob = pawn.GetLord().LordJob as LordJob_Joinable_GuidedMuseumTour;
            if(tourJob == null)
            {
                return null;
            }

            Building donationBox = tourJob.TourData.MuseumRoom?.ContainedAndAdjacentThings?.FirstOrDefault(t => t.def == Common.DonationBoxThingDef) as Building;
            if (donationBox == null)
            {
                return null;
            }
            if (!pawn.CanReserveAndReach(donationBox, PathEndMode.ClosestTouch, Danger.Deadly))
            {
                return null;
            }

            CompThingContainer containerComp = donationBox.GetComp<CompThingContainer>();
            if (containerComp == null)
            {
                return null;
            }

            float currentlyHeldSilver = containerComp.ContainedThing?.stackCount ?? 0;
            float maxHeldSilver = containerComp.Props.stackLimit;
            if (currentlyHeldSilver >= maxHeldSilver)
            {
                return null;
            }

            Thing silverToDonate = silverThing.SplitOff(silverAmountInt);
            pawn.carryTracker.TryStartCarry(silverToDonate);
            Job donationJob = HaulAIUtility.HaulToContainerJob(pawn, silverToDonate, donationBox);
            if (donationJob == null)
            {
                return null;
            }
            donationJob.count = silverToDonate.stackCount;
            guestTracker.Notify_GuestDonated(pawn);
            if (DebugActions.IsDebugActive)
                Log.Message($"Created donation job: {donationJob}, with silver count: {donationJob.count}");
            return donationJob;
        }
    }
}
