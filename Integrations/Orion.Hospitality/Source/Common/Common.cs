﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace MuseumsIntegration_Hospitality
{
    public class Common
    {
        //public static JobDef DonationJobDef = DefDatabase<JobDef>.GetNamed("Museums_DonateToMuseum");
        public static ThingDef DonationBoxThingDef = DefDatabase<ThingDef>.GetNamed("Museums_DonationBox");
    }
}
