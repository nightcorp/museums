﻿#if !v1_4
using LudeonTK;
#endif
using HarmonyLib;
using Museums;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;

namespace MuseumsIntegration_Hospitality.Debug
{
    public class DebugActions
    {
        [DebugAction("Museums", "Add 20 donation", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void Debug_Add20Donation()
        {
            var position = UI.MouseCell();
            var map = Find.CurrentMap;
            var box = position.GetFirstThing(map, Common.DonationBoxThingDef);
            var containerComp = box.TryGetComp<CompThingContainer>();
            containerComp.ContainedThing.stackCount += 20;
        }
    }
}
