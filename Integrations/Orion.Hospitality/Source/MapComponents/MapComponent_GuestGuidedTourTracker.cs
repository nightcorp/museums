﻿using HarmonyLib;
using Museums;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace MuseumsIntegration_Hospitality
{
    public class MapComponent_GuestGuidedTourTracker : MapComponent
    {
        const int guidedGuestTourCooldown = GenDate.TicksPerDay;
        int lastTourTick = -1;
        List<Pawn> guestsThatHaveDonated = new List<Pawn>();
        int nextDonationCacheEvictionTick = -1;
        const int ticksToEvictDonationCache = GenDate.TicksPerQuadrum;

        public MapComponent_GuestGuidedTourTracker(Map map) : base(map) { }

        public int TicksUntilNextTourAvailable
        {
            get
            {
                if(lastTourTick == -1)
                {
                    return 0;
                }
                return Mathf.Max(0, guidedGuestTourCooldown - (GenTicks.TicksGame - lastTourTick));
            }
        }

        public bool CanStartGuidedGuestTour => TicksUntilNextTourAvailable == 0;

        public void Notify_GuestTourStarted()
        {
            lastTourTick = GenTicks.TicksGame;
        }

        public void Notify_GuestDonated(Pawn pawn)
        {
            guestsThatHaveDonated.Add(pawn);
        }

        public bool HasGuestDonated(Pawn pawn)
        {
            return guestsThatHaveDonated.Contains(pawn);
        }

        public override void MapComponentTick()
        {
            base.MapComponentTick();
            if(nextDonationCacheEvictionTick == -1)
            {
                nextDonationCacheEvictionTick = GenTicks.TicksGame + ticksToEvictDonationCache;
            }
            if(GenTicks.TicksGame > nextDonationCacheEvictionTick)
            {
                guestsThatHaveDonated.Clear();
                nextDonationCacheEvictionTick = -1;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref lastTourTick, nameof(lastTourTick));
            Scribe_Collections.Look(ref guestsThatHaveDonated, nameof(guestsThatHaveDonated), LookMode.Reference);
            Scribe_Values.Look(ref nextDonationCacheEvictionTick, nameof(nextDonationCacheEvictionTick));
            if (Scribe.mode == LoadSaveMode.PostLoadInit)
            {
                if(guestsThatHaveDonated == null)
                {
                    guestsThatHaveDonated = new List<Pawn>();
                }
                int removedGuests = guestsThatHaveDonated.RemoveAll(p => p == null || p.Discarded || p.Dead);
                if(DebugActions.IsDebugActive && removedGuests > 0)
                {
                    Log.Message($"Removed {removedGuests} guest entries due to becoming invalid");
                }
            }
        }
    }
}
