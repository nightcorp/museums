﻿using HarmonyLib;
using Museums;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using VFEEmpire;

namespace MuseumsIntegration_VFEEmpire
{
    [HarmonyPatch(typeof(MapComponent_RoyaltyTracker))]
    public static class Patch_MapComponent_RoyaltyTracker
    {
        [HarmonyPatch(nameof(MapComponent_RoyaltyTracker.Notify_UpdateRoomRole))]
        [HarmonyPostfix]
        public static void SubscribeMuseumAsGallery(Room room, RoomRoleDef role, MapComponent_RoyaltyTracker __instance)
        {
            if(role == Common.RoomRole_Museum)
            {
                __instance.Galleries.Add(room);
                //Log.Message($"added museum room {room.ID} as gallery");
            }
        }

        [HarmonyPatch(nameof(MapComponent_RoyaltyTracker.FinalizeInit))]
        [HarmonyPostfix]
        public static void CheckMapForAllMuseums(MapComponent_RoyaltyTracker __instance)
        {
            foreach (Room room in __instance.map.regionGrid.allRooms)
            {
                if(room.Role == Common.RoomRole_Museum)
                {
                    if (!__instance.Galleries.Contains(room))
                    {
                        __instance.Galleries.Add(room);
                        //Log.Message($"added museum room {room.ID} as gallery");
                    }
                }
            }
        }
    }
}
