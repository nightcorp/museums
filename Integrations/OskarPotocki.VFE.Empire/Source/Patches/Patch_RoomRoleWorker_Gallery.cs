﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using VFEEmpire;

namespace MuseumsIntegration_VFEEmpire
{
    [HarmonyPatch(typeof(RoomRoleWorker_Gallery), nameof(RoomRoleWorker_Gallery.GetScore))]
    public static class Patch_RoomRoleWorker_Gallery
    {
        [HarmonyPrefix]
        public static bool DisableGalleryRoomRole(float __result)
        {
            __result = 0;
            return false;
        }
    }
}
