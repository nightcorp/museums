﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace MuseumsIntegration_VFEEmpire
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony harmony = new Harmony(nameof(MuseumsIntegration_VFEEmpire));
            Log.Message($"Empire integration");
            harmony.PatchAll();
        }
    }
}
