﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace MuseumsIntegration_MlieAquarium
{
    public class MuseumsIntegration_MlieAquariumMod : Mod
    {
        public MuseumsIntegration_MlieAquariumMod(ModContentPack content) : base(content)
        {
            Harmony harmony = new Harmony("MuseumsIntegration_MlieAquarium");
            harmony.PatchAll();
        }
    }

}
