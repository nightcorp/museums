﻿using Aquarium;
using HarmonyLib;
using Museums;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Verse;

namespace MuseumsIntegration_MlieAquarium
{
    /// <summary>
    /// Aquarium locks down the CompAquarium pretty tightly with internal and private. No way to get the state of the aquarium without reflection. Oh well.
    /// </summary>
    [HarmonyPatch(typeof(ContainerUtility))]
    public class Patch_ContainerUtility
    {
        [HarmonyPatch(nameof(ContainerUtility.IsContainer))]
        [HarmonyPostfix]
        public static void CheckIsDeepStorageContainer(ref bool __result, ThingDef thingDef)
        {
            if (__result)
            {
                return;
            }
            __result = thingDef.HasAssignableCompFrom(typeof(CompAquarium));
        }

        [HarmonyPatch(nameof(ContainerUtility.ContainsItem))]
        [HarmonyPostfix]
        public static void CheckDeepStorageContainsItem(ref bool __result, ThingWithComps thing)
        {
            if (__result)
            {
                return;
            }
            CompAquarium aquariumComp = thing.GetComp<CompAquarium>();
            if (aquariumComp == null)
            {
                return;
            }
            __result = GetNumberOfFishIn(aquariumComp) > 0;
        }

        static FieldInfo numFishFI = AccessTools.Field(typeof(CompAquarium), "numFish");
        private static int GetNumberOfFishIn(CompAquarium compAquarium)
        {
            return (int)numFishFI.GetValue(compAquarium);
        }
    }
}
