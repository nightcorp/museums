﻿using HarmonyLib;
using LWM.DeepStorage;
using Museums;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace MuseumsIntegration_DeepStorage
{
    [HarmonyPatch(typeof(ContainerUtility))]
    public class Patch_ContainerUtility
    {
        [HarmonyPatch(nameof(ContainerUtility.IsContainer))]
        [HarmonyPostfix]
        public static void CheckIsDeepStorageContainer(ref bool __result, ThingDef thingDef)
        {
            if (__result)
            {
                return;
            }
            __result = thingDef.HasAssignableCompFrom(typeof(LWM.DeepStorage.Properties));
        }

        /// <summary>
        /// Deep storage logic is a mess. Things are not grouped or counted and persisted anywhere, it basically just forces things to be at the position of the container. So the thing list is the only usable part to determine emptiness
        /// </summary>
        /// <param name="__result"></param>
        /// <param name="thing"></param>
        [HarmonyPatch(nameof(ContainerUtility.ContainsItem))]
        [HarmonyPostfix]
        public static void CheckDeepStorageContainsItem(ref bool __result, ThingWithComps thing)
        {
            if (__result)
            {
                return;
            }
            CompDeepStorage deepStorageComp = thing.GetComp<LWM.DeepStorage.CompDeepStorage>();
            if(deepStorageComp == null)
            {
                return;
            }
            __result = AnyItemStoredAt(thing.Position, thing.Map);
        }

        private static bool AnyItemStoredAt(IntVec3 position, Map map)
        {
            foreach (Thing potentiallyStoredThing in position.GetThingList(map))
            {
                if (potentiallyStoredThing.def.EverStorable(false))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
