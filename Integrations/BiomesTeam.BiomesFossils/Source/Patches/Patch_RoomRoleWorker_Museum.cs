﻿using HarmonyLib;
using Museums;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;
using Verse.Noise;

namespace MuseumsIntegration_BiomesFossils
{
    [HarmonyPatch(typeof(BMT_Fossils.RoomRoleWorker_Museum), nameof(BMT_Fossils.RoomRoleWorker_Museum.GetScore))]
    public static class Patch_RoomRoleWorker_Museum
    {
        [HarmonyPrefix]
        public static bool InvalidateFossilsMuseum(ref float __result)
        {
            __result = 0;
            return false;
        }
    }
}
