﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace MuseumsIntegration_BiomesFossils
{
    [HarmonyPatch(typeof(BMT_Fossils.JoyGiver_VisitMuseum), nameof(BMT_Fossils.JoyGiver_VisitMuseum.TryGiveJob))]
    public class Patch_JoyGiver_VisitMuseum
    {
        [HarmonyPrefix]
        public static bool InvalidateBMTFossilsMuseumJoyGiver(Job __result)
        {
            __result = null;
            return false;
        }
    }
}
