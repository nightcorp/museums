﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace MuseumsIntegration_EllitArcheology
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony harmony = new Harmony(nameof(MuseumsIntegration_EllitArcheology));
            harmony.PatchAll();
        }
    }
}
